package john.java.learning.nio.udemy;

import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;

public class TreeCopier extends SimpleFileVisitor<Path> {
	private Path sourceRoot;
	private Path targetRoot;
	
	/* copying
	 * source path = TestFiles/SubTest/subtester.txt
	 * source root = TestFiles
	 * target root = TestFiles/CopyDir
	 * source relativized path = SubTest/subtester.txt
	 * target resolved path = TestFiles/CopyDir/SubTest/Subtester.txt
	 */
	
	public TreeCopier(Path source, Path target){
		sourceRoot = source;
		targetRoot = target;
	}
	
	@Override
	public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
		Path relativizedPath = sourceRoot.relativize(file);
		System.out.println("Relativized path : "+relativizedPath);
		Path copyDir = targetRoot.resolve(relativizedPath);
		System.out.println("Copy directory path : "+copyDir);
		try {
			Files.copy(file, copyDir, REPLACE_EXISTING);
		}catch(IOException e) {
			System.out.println("Error in copying file: " + e.getMessage());
		}
		return FileVisitResult.CONTINUE;
	}
	
	@Override
	public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException{
		if(targetRoot.equals(dir)) { //avoid cycles otherwise endless copying
			return FileVisitResult.CONTINUE;
		}
		Path relativizedPath = sourceRoot.relativize(dir);
		System.out.println("Relativized path : "+relativizedPath);
		Path copyDir = targetRoot.resolve(relativizedPath);
		System.out.println("Copy directory path : "+copyDir);
		try {
			Files.copy(dir, copyDir, REPLACE_EXISTING);
		}catch(IOException e) {
			System.out.println("Error in copying directory : " + e.getMessage());
			return FileVisitResult.SKIP_SUBTREE;
		}
		return FileVisitResult.CONTINUE;
	}
	
	@Override 
	public FileVisitResult visitFileFailed(Path path , IOException e) throws IOException{
		System.out.println("Error in accessing file : "+e.getMessage());
		return FileVisitResult.CONTINUE;
	}
}
