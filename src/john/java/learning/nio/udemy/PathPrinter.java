package john.java.learning.nio.udemy;

import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;

public class PathPrinter extends SimpleFileVisitor<Path> {
	@Override
	public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
		System.out.println("File : " + file.toAbsolutePath());
		return FileVisitResult.CONTINUE;
	}
	
	@Override
	public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException{
		System.out.println("Directory : " + dir.toAbsolutePath());
		return FileVisitResult.CONTINUE;
	}
	
	@Override 
	public FileVisitResult visitFileFailed(Path path , IOException e) throws IOException{
		System.out.println("Error in accessing file : "+e.getMessage());
		return FileVisitResult.CONTINUE;
	}
}
