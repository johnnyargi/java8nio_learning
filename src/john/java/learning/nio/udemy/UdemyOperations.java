package john.java.learning.nio.udemy;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.file.DirectoryIteratorException;
import java.nio.file.DirectoryStream;
import java.nio.file.FileStore;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.List;

//Singleton
public class UdemyOperations {
	public static final UdemyOperations operator = new UdemyOperations();
	
	private UdemyOperations() {
		
	}
	
	public void readFileByLines(Path path) {
		try {
			List<String> lines = Files.readAllLines(path);	//entire file in memory. Not suitable for large files
			for(String line : lines) {
				System.out.println(line);
			}
		}catch(IOException e) {
			System.out.format("Error in reading file : %s\n", e);
		}
	}
	
	public void appendData(Path path, String data) {
		try {
			Files.write(path, data.getBytes("UTF-8"), StandardOpenOption.APPEND);
		}catch(IOException e) {
			System.out.format("Error in reading file : %s\n", e);
		}
	}
	
	public void writeBinaryDataTest(Path path) {
		String fileName = File.separator+"data.dat";
		if(Files.exists(path)) {
			try(FileOutputStream outStream = new FileOutputStream(path.toString() + fileName);
				FileChannel outChannel = outStream.getChannel()){
				byte textBytes[] = "Hello data ".getBytes();
				ByteBuffer buffer = ByteBuffer.wrap(textBytes);
				int numBytes  = outChannel.write(buffer);
				System.out.format(" %s bytes written\n", numBytes);
				ByteBuffer intBuf = ByteBuffer.allocate(Integer.BYTES);
				intBuf.putInt(12);
				intBuf.flip();
				numBytes = outChannel.write(intBuf);
				System.out.format(" %s bytes written\n", numBytes);
				intBuf.flip();	//reset position to write at the beginning. Buffer only fits one int so far
				intBuf.putInt(25);
				intBuf.flip();	//reset position to read from the beginning
				numBytes = outChannel.write(intBuf);
				System.out.format(" %s bytes written\n", numBytes);
				System.out.println("Saved file "+ path.toString() + fileName);
				
//				//Read what was written with IO
//				RandomAccessFile raFile = new RandomAccessFile(path.toString() + fileName, "rwd");
//				byte readBytes[] = new byte[textBytes.length];
//				raFile.read(readBytes);
//				System.out.println(new String(readBytes));
//				System.out.println(raFile.readInt());
//				System.out.println(raFile.readInt());
//				raFile.close();
				
				//Read what was written with NIO
				RandomAccessFile raFile = new RandomAccessFile(path.toString() + fileName, "rwd");
				FileChannel readChannel = raFile.getChannel();
				byte readBytes[] = new byte[textBytes.length];
				ByteBuffer readBuf = ByteBuffer.wrap(readBytes);
				readChannel.read(readBuf);
				System.out.println(new String(readBytes));
				ByteBuffer intReader = ByteBuffer.allocate(Integer.BYTES);
				readChannel.read(intReader);
				intReader.flip();
				System.out.println(intReader.getInt());
				intReader.clear();
				readChannel.read(intReader);
				intReader.flip();
				System.out.println(intReader.getInt());
				readChannel.close();
				raFile.close();
			}catch(IOException e) {
				System.out.format("Error in writing file : %s\n", e);
			}
		}else {
			System.out.println("Invalid path");
		}
	}
	
	public void directoryTest(Path path) {
		DirectoryStream.Filter<Path> filter = new DirectoryStream.Filter<Path>() {
			@Override
			public boolean accept(Path entry) throws IOException {
				return Files.isRegularFile(entry);
			}
		};
		try(DirectoryStream<Path> contents = Files.newDirectoryStream(path, filter)){
			for(Path iter : contents) {
				System.out.println("File name : "+ iter.getFileName() + " File System : " + iter.getFileSystem());
			}
		}catch(IOException | DirectoryIteratorException e) {	//catch both exceptions with pipe character (bitwise or)
			System.out.format("Error in directory test : %s\n", e);
		}
	}
	
	public void printDrives() {
		Iterable<FileStore> fileStores = FileSystems.getDefault().getFileStores();
		for(FileStore fs : fileStores) {
			System.out.println("Store : "+fs);
			System.out.println("Store name : "+ fs.name());
		}
	}
}
