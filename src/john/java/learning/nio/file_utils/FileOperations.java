package john.java.learning.nio.file_utils;

import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;

import java.io.IOException;
import java.nio.file.DirectoryNotEmptyException;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.nio.file.attribute.BasicFileAttributes;

//Singleton
public final class FileOperations {
	public static final FileOperations operator = new FileOperations();
	
	private FileOperations() {
		
	}
	
	public void checkFile(Path path){
		boolean readable;
		boolean writable;
		boolean executable;
		
		System.out.format("The path is : %s\n", path);
		
		if(Files.exists(path)) {
			System.out.println("The File exists");
			System.out.println("Checking file permisssions : ");
			readable = Files.isReadable(path);
			writable = Files.isWritable(path);
			executable = Files.isExecutable(path);
			System.out.format("Readable : %s\nWritable : %s\nExecutable %s\n", readable, writable, executable);
		}
		if(Files.notExists(path)) {
			System.out.println("The File does not exist");
		}
	}
	
	public void deleteFile(Path path) {
		try {
			Files.delete(path);
		}catch(NoSuchFileException x) {
			System.err.format("The file does not exist : %s\n", x);
		}catch(DirectoryNotEmptyException d) {
			System.err.format("Non empty directory : %s\n", d);
		}catch(IOException e) {
			System.err.format("IO error : %s\n",e);
		}
	}
	
	public void copyFile(Path path, Path path2) {
		try {
			Files.copy(path, path2, REPLACE_EXISTING);
		} catch (IOException e) {
			System.err.format("Error in copying : %s\n", e);
		}
	}
	
	public void moveFile(Path path, Path path2){
		try {
			Files.move(path, path2, REPLACE_EXISTING);
		} catch (IOException e) {
			System.err.format("Error in moving : %s\n", e);
		}
	}
	
	public void printFileAttributes(Path path) {
		try {
			BasicFileAttributes attrs = Files.readAttributes(path, BasicFileAttributes.class);
			System.out.println("File attributes =====");
			System.out.println("File size : "+ attrs.size());
			System.out.println("Creation time : " + attrs.creationTime());
			System.out.println("lastAccessTime: " + attrs.lastAccessTime());
			System.out.println("lastModifiedTime: " + attrs.lastModifiedTime());
			System.out.println("isDirectory: " + attrs.isDirectory());
			System.out.println("isOther: " + attrs.isOther());
			System.out.println("isRegularFile: " + attrs.isRegularFile());
			System.out.println("isSymbolicLink: " + attrs.isSymbolicLink());
			
		} catch (IOException e) {
			System.err.format("Error in getting attributes : %s\n", e);
		} 
		
	}
}
