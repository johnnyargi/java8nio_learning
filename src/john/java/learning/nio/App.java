package john.java.learning.nio;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import john.java.learning.nio.file_utils.FileOperations;
import john.java.learning.nio.udemy.PathPrinter;
import john.java.learning.nio.udemy.TreeCopier;
import john.java.learning.nio.udemy.UdemyOperations;

public class App {
	private enum TestType{
		FILE_SYSTEM_TEST,
		FILE_OP_CHARS_TEST,
		FILE_OP_BIN_TEST,
		DIRECTORY_TEST,
		TREE_COPY_TEST
	};
	
	public static void main(String args[]) {
		TestType testType = TestType.TREE_COPY_TEST;
		UdemyOperations uoperator = UdemyOperations.operator;
		FileOperations operator = FileOperations.operator;
		
		System.out.println("Java NIO Testing App");
		
		switch(testType) {
		case FILE_SYSTEM_TEST:
			String fileRoot = "/home/primary12/Desktop/";
			String filePath = fileRoot + "doodle.png";
			String filePath2 = fileRoot + "doodle2.png";
			
			Path doodlePath = Paths.get(filePath);
			Path doodlePath2 = Paths.get(filePath2);
			
			operator.checkFile(doodlePath);
			operator.checkFile(doodlePath2);
			operator.copyFile(doodlePath, doodlePath2);
			operator.checkFile(doodlePath2);
			operator.moveFile(doodlePath2, Paths.get(fileRoot + "Test"+File.separator+"doodle3.png"));
			operator.copyFile(Paths.get(fileRoot + "Test"+File.separator+"doodle3.png"), Paths.get(fileRoot + "Test"+File.separator+"doodle4.png"));
			operator.checkFile(doodlePath2);
			operator.checkFile(Paths.get(fileRoot + "Test"+File.separator+"doodle3.png"));
			operator.checkFile(Paths.get(fileRoot + "Test"+File.separator+"doodle4.png"));
			operator.deleteFile(Paths.get(fileRoot + "Test"+File.separator+"doodle4.png"));
			operator.checkFile(Paths.get(fileRoot + "Test"+File.separator+"doodle4.png"));
			operator.printFileAttributes(doodlePath);
			break;
		case FILE_OP_CHARS_TEST:
			//Udemy part
			Path dataPath = FileSystems.getDefault().getPath("TestFiles"+File.separator+"data.txt");
			uoperator.readFileByLines(dataPath);
			uoperator.appendData(dataPath, "\nExtra");
			break;
		case FILE_OP_BIN_TEST:
			Path binPath = FileSystems.getDefault().getPath("TestFiles");
			uoperator.writeBinaryDataTest(binPath);
			break;
		case DIRECTORY_TEST:
			Path directoryPath = FileSystems.getDefault().getPath("TestFiles");
			uoperator.directoryTest(directoryPath);
			System.out.println("Printing drives *******");
			uoperator.printDrives();
			System.out.println("Printing file tree *******");
			PathPrinter tree = new PathPrinter();
			try {
				Files.walkFileTree(directoryPath, tree);
			}catch(IOException e) {
				System.out.println("Error in printing file tree : "+ e.getMessage());
			}
			break;
		case TREE_COPY_TEST :
			Path source = FileSystems.getDefault().getPath("TestFiles");
			Path dest = FileSystems.getDefault().getPath("TestFiles"+File.separator+"CopyDir");
			System.out.println("Copying Tree *******");
			TreeCopier treeCp = new TreeCopier(source, dest);
			try {
				Files.walkFileTree(source, treeCp);
			}catch(IOException e) {
				System.out.println("Error in printing file tree : "+ e.getMessage());
			}
			break;
		default:
			break;
		}
		
	}
}
